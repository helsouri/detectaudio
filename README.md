# audioCheck

## Problem Statement

I have a habbit of finding funny webms (gifs with sound) and collecting them.<br>
The issue starts by explaining that a GIF can be intesive to process by a website and hence webm containers were made as a flexible container for all media.<br> 
The process of filtering through webms that contain audio vs webm without audio (GIF alternative) can be very extensive and hence I made a script to automatically do that. <br>

## Procedure
The script utalizes the library pymediainfo to detect the number of tracks in a file and delete webms that have no audio.<br>
The Script can also delete non-webm files<br>
All that is needed is any python intallation (tested on 3+ because support for python2 stopped).<br>
Visit: https://pypi.org/project/pymediainfo/ for information on the only extra module needed.<br>
Download script to any location make sure you have all needed modules and run.<br>
It will interactively ask for directory to where media is (make sure all your media is in one directory).<br>
Added batch file to quickly run script on windows. <br>
Script can detect any video file (according to defined set of allowed extensiuons) <br>
Console will show if a file is skipped (meaning it is a video with audio)<br>

## Requirements
pymediainfo as a python repository and mediaInfo installed on machine<br>
mkvtoolknix GUI installed on machine<br>
tk or sudo apt-get install python3-tk (for ubuntu)
