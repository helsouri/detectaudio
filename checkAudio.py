from pymediainfo import MediaInfo           # MediaInfo Library 
from tkinter import filedialog              # Tkinter library to get path using dialog (available by default)
from tkinter import Tk
import glob                                 # glob module for file easy type filtering
import os                                   # os module for easy file/folder pathing

keptCount = 0
durationLimit = 15
renamedCount = 0
deletedCount = {"wrongType" : 0, "noAudio" : 0, "tooShort" : 0, "resolution" : 0}
userChoice = 0

class myFileClass():

    # initilize class
    def __init__(self):
        self.hasAudio = False
        self.videoDuration = 0.0
        self.videoSize = {"width" : 0 , "length" : 0}
        self.needToRename = False
        self.trackTitle = ""
        self.trackFileName = ""
        self.durationPass = False
        self.resolutionPass = False

    def setHasAudio(self):
        self.hasAudio = True

    def setDuration(self, tempTrackDuration:float):
        self.videoDuration = tempTrackDuration/1000
    
    def setVideoSize(self, tempTrackWidth, tempTrackHeight):
        self.videoSize["width"] = tempTrackWidth
        self.videoSize["length"] = tempTrackHeight

    def setNeedToRename(self, tempTrackTitle, tempFileName):
        self.needToRename = True
        self.trackTitle = tempTrackTitle
        self.trackFileName = tempFileName

    def doRename(self, tempFile):
        print("Title \"{}\" will now match file name \"{}\"".format(self.trackTitle, self.trackFileName))
        os.system("mkvpropedit \"{}\" --edit info --set \"title={}\"".format(tempFile, self.trackFileName))
    
    def doActions(self, tempFile, tempChoice:int , tempRenamedCount:int, tempKeptCount:int, tempDeletedCount:dict ):

        if(not self.hasAudio):
            print(thisFile, "will be deleted since it doesnt have audio")
            tempDeletedCount["noAudio"]+=1
            os.remove (tempFile)

        else:
            if(tempChoice==1):
                self.doRename(tempFile)
                tempRenamedCount+=1
            elif(tempChoice==2):

                self.doRename(tempFile)
                tempRenamedCount+=1

                if(self.videoDuration <= durationLimit):
                    if(os.path.exists(tempFile)):
                        print("{} will be deleted since it is shorter than {} seconds".format(tempFile,durationLimit))
                        deletedCount["tooShort"]+=1
                        os.remove(tempFile)
                else:
                    self.durationPass = True
                
                if(self.videoSize["length"] <= 200 or self.videoSize["width"] <= 200 or self.videoSize["width"]<self.videoSize["length"]): #check file resoultuion to be more than 200x200
                    if(os.path.exists(tempFile)):
                        print(tempFile, "will be deleted since it due to resolution")
                        deletedCount["resolution"]+=1
                        os.remove(tempFile)
                else:
                    self.resolutionPass = True

                if(self.durationPass and self.resolutionPass):
                    print(tempFile, "will be kept.")
                    tempKeptCount+=1
            else:
                
                if(self.videoDuration <= durationLimit):
                    if(os.path.exists(tempFile)):
                        print("{} will be deleted since it is shorter than {} seconds".format(tempFile,durationLimit))
                        deletedCount["tooShort"]+=1
                        os.remove(tempFile)
                else:
                    self.durationPass = True
                
                if(self.videoSize["length"] <= 200 or self.videoSize["width"] <= 200 or self.videoSize["width"]<self.videoSize["length"]): #check file resoultuion to be more than 200x200
                    if(os.path.exists(tempFile)):
                        print(tempFile, "will be deleted since it due to resolution")
                        deletedCount["resolution"]+=1
                        os.remove(tempFile)
                else:
                    self.resolutionPass = True

                if(self.durationPass and self.resolutionPass):
                    print(tempFile, "will be kept.")
                    tempKeptCount+=1



def giveChoice():
    thisChoice = input("Press 1 to only match file name with file track name\n"
                      +"Press 2 to match naming and do other checks(video duration, audio presence, video resolution)\n"
                      +"Press 3 to only do other checks (skip name matching)\n"
                      + "Choice: " )
    return int(thisChoice)

def setDurationLimit():
    tempDurationLimit = durationLimit
    thisChoice = input("\nBy default any video shorter than {} seconds will be deleted.\nDo you want to change that limit? (Y/N): \t".format(tempDurationLimit))
    if thisChoice.upper() == "Y":
        tempDurationLimit = int(input("Enter new duration limit in seconds: \t"))
    else:
        print("You have not changed the duration limit.")
    return tempDurationLimit

videoExtensions = ['mp4', 'webm', 'ts', 'mkv']



root = Tk()                                 # create a dialog in pwd 
root.withdraw()                             # do not show gui
folderSelected = filedialog.askdirectory()  # use to select path

userChoice = giveChoice()

if (userChoice != 1):
    durationLimit = setDurationLimit()



for thisFile in os.listdir(folderSelected):
    thisFileObject = myFileClass()

    fileFullPath = folderSelected + '/' + thisFile
    if(os.path.isfile(fileFullPath)):
        fileExtention = thisFile [thisFile.rfind (".") + 1:] # get extension
        if (fileExtention not in videoExtensions) : 
            print(thisFile, "will be deleted since it is not a video.")
            deletedCount["wrongType"]+=1
            os.remove (fileFullPath)
        else:
            fileMetadataInfo = MediaInfo.parse(fileFullPath)

            for track in fileMetadataInfo.tracks:
                if track.track_type == "Audio":
                    thisFileObject.setHasAudio()

                if track.track_type == "Video":
                    thisFileObject.setDuration(float(track.duration))
                    thisFileObject.setVideoSize(tempTrackWidth=track.width,tempTrackHeight=track.height)

                if (track.track_type == "General" and track.title != track.file_name):
                    thisFileObject.setNeedToRename(tempTrackTitle=track.title, tempFileName=track.file_name)
            thisFileObject.doActions(tempFile=fileFullPath,tempChoice=userChoice,tempRenamedCount=renamedCount,tempKeptCount=keptCount,tempDeletedCount=deletedCount)   

print(  "There is a total of {} files".format(len(glob.glob( os.path.abspath (folderSelected+"/*"))))
        + "\nKept {} files\n".format(keptCount)
        + "Renamed {} files\n".format(renamedCount)
        + "Deleted {} file(s) that are non-webm.\n".format(deletedCount["wrongType"])
        + "Deleted {} file(s) that have no audio\n".format(deletedCount["noAudio"])
        + "Deleted {} file(s) that are shorter than {} seconds".format(deletedCount["tooShort"], durationLimit)
        +"\nDeteled {} file(s) due to resolution".format(deletedCount["resolution"])
        + "\nNo other files to remove.")